package org.vsklamm.openway;

import org.junit.Test;

import java.io.*;
import java.util.Random;

public class LabyrinthTest {

    private void runTest(String file) {
        Labyrinth lab = new Labyrinth();
        File outputFile = getOutFile(file);
        try {
            outputFile.createNewFile();
            lab.run(
                    new FileInputStream(getInFile(file)),
                    new FileOutputStream(outputFile, false)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void runResTest(String file) {
        Labyrinth lab = new Labyrinth();
        File outputFile = getOutFile(file);
        try {
            outputFile.createNewFile();
            lab.run(
                    getClass().getClassLoader().getResourceAsStream(file + ".txt"),
                    new FileOutputStream(outputFile, false)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEmpty() {
        runResTest("zero");
    }

    @Test
    public void testSingle() {
        runResTest("single");
    }

    @Test
    public void testSmall() {
        runResTest("small");
    }

    @Test
    public void testMedium() {
        runResTest("medium");
    }

    @Test
    public void testBig() {
        runResTest("big");
    }

    @Test
    public void testVeryLarge() {
        String file = "large";
        try {
            MazeGenerator.generate(
                    15,
                    700,
                    900,
                    new FileOutputStream(getInFile(file), false)
            );
            runTest(file);
        } catch (FileNotFoundException e) {
            System.out.println("Oops");
        }
    }

    @Test
    public void testRandom() {
        Random rand = new Random();
        String file = "random";
        try {
            MazeGenerator.generate(
                    rand.nextInt(20) + 1,
                    rand.nextInt(40) + 1,
                    rand.nextInt(40) + 1,
                    new FileOutputStream(getInFile(file), false)
            );
            runTest(file);
        } catch (FileNotFoundException e) {
            System.out.println("Oops");
        }
    }

    private File getInFile(String file) {
        return new File(file + ".txt");
    }

    private File getOutFile(String file) {
        return new File(file + "_solved.txt");
    }
}
