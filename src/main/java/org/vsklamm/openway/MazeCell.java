package org.vsklamm.openway;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.vsklamm.openway.BaseCell.Barrier.*;

class MazeCell extends BaseCell {

    /**
     * Count of steps from source cell.
     */
    int steps = Integer.MAX_VALUE;

    /**
     * Priority in A Star search
     */
    int priority = Integer.MAX_VALUE;

    /**
     * Stores parent cell for restoring path.
     */
    MazeCell parent = null;

    /**
     * True if maze cell is on the shortest path.
     */
    boolean onPath = false;

    MazeCell(int x, int y) {
        super(x, y);
        reset();
    }

    /**
     * Checks if the cell is at the edge of maze and which walls it has.
     *
     * @return {@link List} of {@link Point} with adjacent reachable cells.
     */
    List<Point> getNeighbours() {
        List<Point> result = new ArrayList<>();
        if (!this.hasBarrier(WEST_WALL) && !this.hasBarrier(WEST_EDGE)) {
            result.add(new Point(cord.x, cord.y - 1));
        }
        if (!this.hasBarrier(NORTH_WALL) && !this.hasBarrier(NORTH_EDGE)) {
            result.add(new Point(cord.x - 1, cord.y));
        }
        if (!this.hasBarrier(EAST_WALL) && !this.hasBarrier(EAST_EDGE)) {
            result.add(new Point(cord.x, cord.y + 1));
        }
        if (!this.hasBarrier(SOUTH_WALL) && !this.hasBarrier(SOUTH_EDGE)) {
            result.add(new Point(cord.x + 1, cord.y));
        }
        return result;
    }

    /**
     * Assigns initial values to MazeCell fields (before running the algorithm).
     */
    void reset() {
        steps = Integer.MAX_VALUE;
        priority = Integer.MAX_VALUE;
        parent = null;
        onPath = false;
    }
}
