package org.vsklamm.openway;

import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static org.vsklamm.openway.BaseCell.Barrier.*;

/**
 * This class solves Shortest Path problem in labyrinth given with certain format
 *
 * @author Sergey Vasilchenko
 * @version 1.1
 * @see Labyrinth#run(InputStream, OutputStream)
 */
public class Labyrinth {

    private ArrayList<ArrayList<MazeCell>> maze;

    /**
     * Main method, that will be executed.
     * This methods starts solving shortest path problem for given graph and points
     *
     * @param args arguments for running the program from the command line.
     * @see Labyrinth#run(InputStream, OutputStream)
     */
    public static void main(String[] args) {
        if (args == null || args.length != 2 || Arrays.stream(args).anyMatch(Objects::isNull)) {
            System.out.println("Incorrect input format.\nUsage: Labyrinth <input file> <output file>");
            return;
        }
        try {
            new Labyrinth().run(new FileInputStream(args[0]), new FileOutputStream(args[1]));
        } catch (FileNotFoundException e) {
            System.out.println("Input file not found");
        }
    }

    private void resetCalculations() {
        for (ArrayList<MazeCell> row : maze) {
            for (MazeCell cell : row) {
                cell.reset();
            }
        }
    }

    private int restorePath(final MazeCell source, MazeCell cell) {
        int distance = 0;
        while (cell != null && !cell.equals(source)) {
            distance++;
            cell.onPath = true;
            cell = cell.parent;
        }
        if (cell != null) {
            cell.onPath = true;
            return distance;
        } else {
            return -1;
        }
    }

    /**
     * Manhattan distance on a square grid
     */
    private int heuristic(MazeCell a, MazeCell b) {
        return Math.abs(a.cord.x - b.cord.x) + Math.abs(a.cord.y - b.cord.y);
    }

    private int findShortestPath(MazeCell source, MazeCell destination) {
        resetCalculations();
        PriorityQueue<MazeCell> frontier = new PriorityQueue<>(Comparator.comparingDouble(o -> o.priority));
        source.steps = 0;
        source.priority = heuristic(destination, source);
        frontier.add(source);

        while (!frontier.isEmpty()) {
            final MazeCell current = frontier.poll();
            if (current.equals(destination)) {
                break;
            }
            for (Point neighCoordinates : current.getNeighbours()) {
                var neigh = maze.get(neighCoordinates.x).get(neighCoordinates.y);
                final int newCost = current.steps + 1;
                if (newCost < neigh.steps) {
                    neigh.parent = current;
                    neigh.steps = newCost;
                    neigh.priority = newCost + heuristic(destination, neigh);
                    frontier.remove(neigh);
                    frontier.add(neigh);
                }
            }
        }
        return restorePath(source, destination);
    }

    private void printSolution(int distance, MazeCell src, MazeCell dest, BufferedWriter writer) throws IOException {
        if (distance == -1) {
            writer.write(String.format(
                    "No path between (%d, %d) and (%d, %d).",
                    src.cord.x + 1, src.cord.y + 1, dest.cord.x + 1, dest.cord.y + 1)
            );
            writer.newLine();
            writer.newLine();
            return;
        }
        writer.write(String.format(
                "Steps from (%d, %d) to (%d, %d): %d.",
                src.cord.x + 1, src.cord.y + 1, dest.cord.x + 1, dest.cord.y + 1,
                distance)
        );
        writer.newLine();
        writer.write("+");
        for (MazeCell cell : maze.get(0)) {
            writer.write(cell.hasBarrier(NORTH_WALL) ? "-+" : " +");
        }
        for (ArrayList<MazeCell> row : maze) {
            writer.newLine();
            writer.write(row.get(0).hasBarrier(WEST_WALL) ? "|" : " ");
            for (MazeCell cell : row) {
                char inCell;
                if (!cell.onPath) {
                    inCell = ' ';
                } else if (cell.cord.equals(src.cord)) {
                    inCell = 'S';
                } else if (cell.cord.equals(dest.cord)) {
                    inCell = 'F';
                } else {
                    inCell = 'o';
                }
                writer.write(inCell + (cell.hasBarrier(EAST_WALL) ? "|" : " "));
            }
            writer.newLine();
            writer.write("+");
            for (MazeCell cell : row) {
                writer.write(cell.hasBarrier(SOUTH_WALL) ? "-+" : " +");
            }
        }
        writer.newLine();
        writer.newLine();
    }

    /**
     * Runs the A* algorithm of finding a path between two cells in a labyrinth {@code maze}
     * and prints maze with the shortest path for each request.
     *
     * @param in  {@link InputStream}, where the maze and search queries are read from.
     * @param out {@link OutputStream}, where the maze and search queries are read from.
     */
    public void run(InputStream in, OutputStream out) {
        try {
            MazeParser mazeParser = new MazeParser();
            mazeParser.parse(in);
            maze = mazeParser.getParsedMaze();
            int[][] queries = mazeParser.getParsedQueries();

            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, StandardCharsets.UTF_8))) {
                for (int[] query : queries) {
                    final int dist = findShortestPath(
                            maze.get(query[0]).get(query[1]),
                            maze.get(query[2]).get(query[3])
                    );
                    printSolution(
                            dist,
                            maze.get(query[0]).get(query[1]),
                            maze.get(query[2]).get(query[3]),
                            writer
                    );
                }
            } catch (IOException e) {
                System.out.println("I/O error occurred: cannot output maze: " + e.getMessage());
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Incorrect query points' coordinates: " + e.getMessage());
            }
        } catch (MazeParseException e) {
            System.out.println("Error while parsing: " + e.getMessage());
        }
    }
}
