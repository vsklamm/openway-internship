package org.vsklamm.openway;

class MazeParseException extends Exception {
    MazeParseException(String errorMessage) {
        super(errorMessage);
    }
}
