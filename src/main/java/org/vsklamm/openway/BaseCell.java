package org.vsklamm.openway;

import java.awt.*;

class BaseCell {

    /**
     * Coordinates of cell in maze
     */
    final Point cord;

    /**
     * 1000'xxxx - is west edge of maze
     * 0100'xxxx - is north edge of maze
     * 0010'xxxx - is east edge of maze
     * 0001'xxxx - is south edge of maze
     * xxxx'1000 - west wall
     * xxxx'0100 - north wall
     * xxxx'0010 - east wall
     * xxxx'0001 - south wall
     */
    private byte barriers = 0;

    BaseCell(final int x, final int y) {
        this.cord = new Point(x, y);
    }

    void setBarrier(Barrier newBarrier) {
        barriers |= (1 << newBarrier.ordinal());
    }

    boolean hasBarrier(Barrier newBarrier) {
        return (barriers & (1 << newBarrier.ordinal())) != 0;
    }

    public enum Barrier {
        WEST_WALL,
        NORTH_WALL,
        EAST_WALL,
        SOUTH_WALL,
        WEST_EDGE,
        NORTH_EDGE,
        EAST_EDGE,
        SOUTH_EDGE
    }
}
