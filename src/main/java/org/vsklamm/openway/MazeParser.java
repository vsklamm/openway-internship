package org.vsklamm.openway;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.vsklamm.openway.BaseCell.Barrier.*;

public class MazeParser {

    private final Pattern patternNorthAndSouth = Pattern.compile("(\\+[s-])+\\+");
    private ArrayList<ArrayList<MazeCell>> maze;
    private int[][] queries;

    private void processRow(List<MazeCell> mazeRow, final String line, final Direction direction) throws MazeParseException {
        Matcher m;
        if (direction != Direction.MIDDLE) {
            m = patternNorthAndSouth.matcher(line);
            if (!m.find()) {
                throw new MazeParseException("Input data is incorrect: wrong maze format");
            }
        }
        int i = direction == Direction.MIDDLE ? 0 : 1;
        final char wall = direction == Direction.MIDDLE ? '|' : '-';
        for (; i < line.length(); i += 2) {
            if (line.charAt(i) == wall) {
                switch (direction) {
                    case NORTH:
                        mazeRow.get(i / 2).setBarrier(NORTH_WALL);
                        break;
                    case MIDDLE:
                        if (i / 2 == 0) {
                            mazeRow.get(i / 2).setBarrier(WEST_WALL);
                        } else if (i / 2 == mazeRow.size()) {
                            mazeRow.get(i / 2 - 1).setBarrier(EAST_WALL);
                        } else {
                            mazeRow.get(i / 2).setBarrier(WEST_WALL);
                            mazeRow.get(i / 2 - 1).setBarrier(EAST_WALL);
                        }
                        break;
                    case SOUTH:
                        mazeRow.get(i / 2).setBarrier(SOUTH_WALL);
                        break;
                }
            }
        }
    }

    public void parse(InputStream in) throws MazeParseException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {
            final String[] mazeSizes = reader.readLine().split("\\s+");
            final int height = Integer.parseInt(mazeSizes[0]);
            final int width = Integer.parseInt(mazeSizes[1]);

            maze = new ArrayList<>(height);
            if (height == 0 || width == 0) {
                queries = new int[0][4];
                return;
            }
            Deque<String> row = new ArrayDeque<>(3);
            row.add(reader.readLine());
            for (int i = 0; i < height; i++) {
                row.add(reader.readLine());
                row.add(reader.readLine());
                ArrayList<MazeCell> mazeRow = new ArrayList<>(width);
                for (int j = 0; j < width; j++) {
                    mazeRow.add(new MazeCell(i, j));
                }
                processRow(mazeRow, row.pop(), Direction.NORTH);
                processRow(mazeRow, row.pop(), Direction.MIDDLE);
                processRow(mazeRow, row.getFirst(), Direction.SOUTH);
                for (int j = 0; j < width; j++) {
                    if (j == 0) {
                        mazeRow.get(j).setBarrier(WEST_EDGE);
                    }
                    if (i == 0) {
                        mazeRow.get(j).setBarrier(NORTH_EDGE);
                    }
                    if (j + 1 == width) {
                        mazeRow.get(j).setBarrier(EAST_EDGE);
                    }
                    if (i + 1 == height) {
                        mazeRow.get(j).setBarrier(SOUTH_EDGE);
                    }
                }
                maze.add(mazeRow);
            }
            final int queriesCount = Integer.parseInt(reader.readLine());
            queries = new int[queriesCount][4];
            for (int i = 0; i < queriesCount; i++) {
                queries[i] = Arrays.stream(reader.readLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
                for (int j = 0; j < queries[i].length; j++) {
                    queries[i][j]--;
                }
            }
        } catch (FileNotFoundException e) {
            throw new MazeParseException("Input file not found: " + e.getMessage());
        } catch (IOException e) {
            throw new MazeParseException("I/O error occurred while reading input data: " + e.getMessage());
        } catch (NumberFormatException e) {
            throw new MazeParseException("Incorrect input data: parse number error occurred: " + e.getMessage());
        } catch (IndexOutOfBoundsException e) {
            throw new MazeParseException("Incorrect input data: wrong maze size: " + e.getMessage());
        } catch (NullPointerException e) {
            throw new MazeParseException("Incorrect input data: wrong queries format: " + e.getMessage());
        }
    }

    public ArrayList<ArrayList<MazeCell>> getParsedMaze() {
        return maze;
    }

    public int[][] getParsedQueries() {
        return queries;
    }

    private enum Direction {
        NORTH,
        MIDDLE,
        SOUTH
    }
}
