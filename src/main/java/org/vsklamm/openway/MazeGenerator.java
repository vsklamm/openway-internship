package org.vsklamm.openway;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static org.vsklamm.openway.BaseCell.Barrier.*;

/**
 * This class generates simple maze with given height and width
 * Algorithm description: http://weblog.jamisbuck.org/2011/1/27/maze-generation-growing-tree-algorithm
 */
public class MazeGenerator {

    public static void main(String[] args) {
        if (args == null || args.length != 4 || Arrays.stream(args).anyMatch(Objects::isNull)) {
            throw new IllegalArgumentException("Incorrect input format.\nUsage: MazeGenerator <queries> <height> <width> <output file>");
        }
        try {
            MazeGenerator.generate(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), new FileOutputStream(args[3]));
        } catch (NumberFormatException e) {
            System.out.println("Incorrect height or width of maze");
        } catch (FileNotFoundException e) {
            System.out.println("Output file not found");
        }
    }

    /**
     * Generates the maze with a given size and print it to given {@link OutputStream}.
     *
     * @param queries number of queries with the following format: {source.x, source.y} {destination.x, destination.y}
     * @param width   width of maze
     * @param height  height of maze
     * @param out     {@link OutputStream}, where the maze is printed
     */
    public static void generate(final int queries, final int height, final int width, OutputStream out) {
        GenCell[][] maze = new GenCell[height][width];
        for (int x = 0; x < height; x++) {
            for (int y = 0; y < width; y++) {
                maze[x][y] = new GenCell(x, y);
            }
        }
        Random rand = new Random();
        final int startX = rand.nextInt(height);
        final int startY = rand.nextInt(width);
        maze[startX][startY].visited = true;

        Deque<GenCell> path = new ArrayDeque<>();
        path.push(maze[startX][startY]);
        while (!path.isEmpty()) {
            GenCell cell = path.getFirst();
            ArrayList<GenCell> nextStep = new ArrayList<>();
            if (cell.cord.x > 0 && !maze[cell.cord.x - 1][cell.cord.y].visited) {
                nextStep.add(maze[cell.cord.x - 1][cell.cord.y]);
            }
            if (cell.cord.x < height - 1 && !maze[cell.cord.x + 1][cell.cord.y].visited) {
                nextStep.add(maze[cell.cord.x + 1][cell.cord.y]);
            }
            if (cell.cord.y > 0 && !maze[cell.cord.x][cell.cord.y - 1].visited) {
                nextStep.add(maze[cell.cord.x][cell.cord.y - 1]);
            }
            if (cell.cord.y < width - 1 && (!maze[cell.cord.x][cell.cord.y + 1].visited)) {
                nextStep.add(maze[cell.cord.x][cell.cord.y + 1]);
            }
            if (!nextStep.isEmpty()) {
                GenCell next = nextStep.get(rand.nextInt(nextStep.size()));
                if (next.cord.x != cell.cord.x) {
                    if (cell.cord.x - next.cord.x > 0) {
                        maze[cell.cord.x][cell.cord.y].setBarrier(WEST_WALL);
                        maze[next.cord.x][next.cord.y].setBarrier(EAST_WALL);
                    } else {
                        maze[cell.cord.x][cell.cord.y].setBarrier(WEST_WALL);
                        maze[next.cord.x][next.cord.y].setBarrier(EAST_WALL);
                    }
                }
                if (next.cord.y != cell.cord.y) {
                    if (cell.cord.y - next.cord.y > 0) {
                        maze[cell.cord.x][cell.cord.y].setBarrier(NORTH_WALL);
                        maze[next.cord.x][next.cord.y].setBarrier(SOUTH_WALL);
                    } else {
                        maze[cell.cord.x][cell.cord.y].setBarrier(SOUTH_WALL);
                        maze[next.cord.x][next.cord.y].setBarrier(NORTH_WALL);
                    }
                }
                maze[next.cord.x][next.cord.y].visited = true;
                path.push(next);
            } else {
                path.pop();
            }
        }
        final int[][] qu = generateQueries(queries, height, width);
        printMazeTask(maze, qu, out);
    }

    private static int[][] generateQueries(final int queries, final int height, final int width) {
        int[][] qu = new int[queries][4];
        Random rand = new Random();
        for (int i = 0; i < queries; i++) {
            qu[i][0] = rand.nextInt(height) + 1;
            qu[i][1] = rand.nextInt(width) + 1;
            qu[i][2] = rand.nextInt(height) + 1;
            qu[i][3] = rand.nextInt(width) + 1;
        }
        return qu;
    }

    private static void printMazeTask(final GenCell[][] maze, final int[][] qu, OutputStream out) {
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, StandardCharsets.UTF_8))) {
            if (maze.length == 0) {
                writer.write("0");
                return;
            }
            writer.write(maze.length + " " + maze[0].length);
            writer.newLine();
            writer.write("+");
            for (GenCell genCell : maze[0]) {
                writer.write(genCell.hasBarrier(NORTH_WALL) ? "-+" : " +");
            }
            for (GenCell[] genCells : maze) {
                writer.newLine();
                writer.write(genCells[0].hasBarrier(WEST_WALL) ? "|" : " ");
                for (GenCell mazeCell : genCells) {
                    writer.write(mazeCell.hasBarrier(EAST_WALL) ? " |" : "  ");
                }
                writer.newLine();
                writer.write("+");
                for (GenCell mazeCell : genCells) {
                    writer.write(mazeCell.hasBarrier(SOUTH_WALL) ? "-+" : " +");
                }
            }
            writer.newLine();
            // queries output
            writer.write(String.valueOf(qu.length));
            writer.newLine();
            if (qu.length == 0) {
                return;
            }
            for (int[] coord : qu) {
                writer.write(String.format("%d %d %d %d", coord[0], coord[1], coord[2], coord[3]));
                writer.newLine();
            }
            System.out.println("Maze generated");
        } catch (IOException e) {
            System.out.println("I/O error occurred: cannot output maze");
        }
    }

    /**
     * Lite version of maze cell used just for maze generating
     */
    private static class GenCell extends BaseCell {
        boolean visited = false;

        GenCell(final int x, final int y) {
            super(x, y);
        }
    }
}
